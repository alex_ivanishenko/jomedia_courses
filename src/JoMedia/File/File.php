<?php

namespace JoMedia\File;

class File implements FileInterface
{
    public $filename;
    public $openfile;
    
    public function __construct($filename, $mode = 'r+')
    {
        if (file_exists($filename)) {
            $this->filename = $filename;
            $this->openfile = fopen($filename, $mode);
        } else {
           die('File doesn`t exist');
        }
    }
    
    public function __destruct()
    {
       $this->filename;
    }    
    
    /**
     * Gets file size.
     *
     * @return int Returns the size of the file in bytes.
     */
    public function getSize()
    {
        if (file_exists($filename)) {
            return filesize($this->filename);
        }
        return false;       
    }

    /**
     * Gets file permissions.
     *
     * @return string Returns a file permissions as letters (e.g. -rwxrw-r--).
     */
    public function getPerms()
    {
        $perms = fileperms($this->filename);

        if (($perms & 0xC000) == 0xC000) {
            // Socket
            $info = 's';
        } elseif (($perms & 0xA000) == 0xA000) {
            // Symbolic Link
            $info = 'l';
        } elseif (($perms & 0x8000) == 0x8000) {
            // Regular
            $info = '-';
        } elseif (($perms & 0x6000) == 0x6000) {
            // Block special
            $info = 'b';
        } elseif (($perms & 0x4000) == 0x4000) {
            // Directory
            $info = 'd';
        } elseif (($perms & 0x2000) == 0x2000) {
            // Character special
            $info = 'c';
        } elseif (($perms & 0x1000) == 0x1000) {
            // FIFO pipe
            $info = 'p';
        } else {
            // Unknown
            $info = 'u';
        }

        // Owner
        $info .= (($perms & 0x0100) ? 'r' : '-');
        $info .= (($perms & 0x0080) ? 'w' : '-');
        $info .= (($perms & 0x0040) ?
                 (($perms & 0x0800) ? 's' : 'x' ) :
                 (($perms & 0x0800) ? 'S' : '-'));

        // Group
        $info .= (($perms & 0x0020) ? 'r' : '-');
        $info .= (($perms & 0x0010) ? 'w' : '-');
        $info .= (($perms & 0x0008) ?
                 (($perms & 0x0400) ? 's' : 'x' ) :
                 (($perms & 0x0400) ? 'S' : '-'));

        // World
        $info .= (($perms & 0x0004) ? 'r' : '-');
        $info .= (($perms & 0x0002) ? 'w' : '-');
        $info .= (($perms & 0x0001) ?
                 (($perms & 0x0200) ? 't' : 'x' ) :
                 (($perms & 0x0200) ? 'T' : '-'));

        return $info;
    }

    /**
     * Tells is a file available for reading.
     *
     * @return bool Returns TRUE if a file is readable, FALSE otherwise.
     */
    public function isReadable()
    {        
        return is_readable($this->filename);        
    }

    /**
     * Tells is a file available for writing.
     *
     * @return bool Returns TRUE if a file is writable, FALSE otherwise.
     */
    public function isWritAble()
    {
        return is_writable($this->filename);
    }

    /**
     * Gets last access time of file
     *
     * @return string Returns last access time of file in (30.12.2015 10:15:59) format
     */
    public function getLastAccessTime()
    {        
        if (file_exists($filename)) {
            return date('d-m-Y H:i:s', fileatime($this->filename));
        }
        return false;                
    }

    /**
     * Locks or unlocks the file.
     *
     * @param $operation
     * LOCK_SH to acquire a shared lock
     * LOCK_EX to acquire an exclusive lock
     * LOCK_UN to release a lock
     *
     * @return bool Returns TRUE on success or FALSE on failure.
     */
    public function lock($operation)
    {
        return flock($this->openfile, $operation);       
    }
}
